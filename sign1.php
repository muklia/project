<!DOCTYPE html>
<html lang="en">

<head>
    <script src="js/angular.min.js"></script>
    <script src="js/script.js"></script>


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop site</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    .formas{
      width: 95%;
      height: 34px;
      padding: 6px 12px;
      font-size: 14px;
      color: #555;
      background-color: #fff;
      border: 1px solid #ccc;
      border-radius: 4px;
      
    }
    </style>

</head>

<body ng-app="OrderList" ng-controller="OrderListController">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Shop</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="index.php">Home</a>
                    </li>
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                    <li>
                      <?php 
                        session_start();
                        if(!empty($_SESSION['login'])){
                          echo "<a href = 'profile.php' class='' style=''>Login:" .$_SESSION['login']. "</a>";
                        }else{
                          echo '<a href="sign1.php">Login or Register</a>';
                        }
                       ?>
                    </li>
                    <li>
                      <?php 
                      if(!empty($_SESSION['login'])){
                          echo "<a href = 'logout.php' class='' style=''>Logout</a>";
                        }
                      ?>
                    </li>
                </ul>
               <!--  <a href="sign1.php"><button type="button" class="btn btn-default navbar-btn navbar-right">Sign</button></a> -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">
        <div class="well"><h3 style="text-align:center; font-weight:bold;">Account</h3></div>
        <div class="col-md-6" id="show1">
          <div class="form-group">
            <label for="exampleInputEmail1"><h2>Sign in</h2></label>
            <input type="text" ng-model = "llogin" name = "login" class="form-control" id="exampleInputEmail1" placeholder="Login" required autofocus>
          </div>
          <div class="form-group">
            <input type="password" ng-model="ppassword" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required autofocus>
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox"> Check me out
            </label>
          </div>
          <button type="submit" ng-click="content()" class="btn btn-default">Submit</button>
        
        </div>
        <div class="col-md-6" id="show2">
          <div class="form-group">
            <label for="exampleInputEmail1"><h2 style="">Register</h2></label>
            <input type="text" ng-model="regname" class="formas" style="" id="ex" name="name" placeholder="Name" required autofocus>
            
          </div>
          <div class="form-group">
            <input type="text" ng-model="regsurname" class="formas" id="exampleInputEmail1"  name="surname"  placeholder="Surname" required autofocus>
          </div>
          <div class="form-group">
            <input type="email" ng-model="regemail" class="formas" id="exampleInputEmail1"  name="email"  placeholder="email" required autofocus>
          </div>
          <div class="form-group">
            <input type="text" ng-model="reglogin" class="formas" id="ex1" name="login"  placeholder="login" ng-model="area" required autofocus >
            <span ng-model="counter" id="value" style="font-weight:bold;color:red;">{{99-area.length}}</span>
          </div>
          <div class="form-group">
            <input type="password" ng-model="regpassword" class="formas" id="ex2" ng-model="area2" name="password"  placeholder="Password" required autofocus>
            <span ng-model="counter" id="value1" style="font-weight:bold;color:red;">{{99-area2.length}}</span>
          </div>
          <div class="form-group">
            <input type="password" ng-change="validation()" ng-model="regpassword1" class="formas" id="exampleInputPassword1" name="repassword"  placeholder="REPassword" required autofocus>
          </div>
          <button type="submit" ng-style="buttonstyle" ng-click="registration()" class="btn btn-default" style="display: none;">Submit</button>
        </div>
    </div><br>
    <div class="container" >

    <div class="col-md-6" style="">
     
            <label for="exampleInputEmail1"><h3>Write some comment:</h3></label>
            <input type="text" ng-model="foremail" id="exampleInputEmail1" placeholder="Write your email" required autofocus><br>
            <textarea type="text" ng-model="forcomment" class="form-control" id="exampleInputPassword1" placeholder="Comment" required autofocus></textarea><br>
            <button type="submit" ng-click="send()" class="btn btn-default">Submit</button>
      
    </div>
      <div class="col-md-6" style=""></div>
      <div class="col-md-6">
       <?php 
          $con=mysqli_connect("localhost","root","","users");
          $query="SELECT * FROM comment";
          $results=$con->query($query);
          while($row = mysqli_fetch_array($results)){
              echo "<div class='col-md-12' style='margin-top:5px; background-color:rgb(245,245,245)'><label><span style='font-size:15px'>User:</span></label><span>".$row['email']."</span><br>
              <label><span style='font-size:15px'>Comment:</span></label><span>".$row['com']."</span></div>";
            }
        ?>
        </div>
        <div class="col-md-3"></div>
      
    </div>
    <!-- /.container -->
    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Shop 2016</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery-2.2.0.js"></script>
    
    <script>
$(document).ready(function(){
    $("#show1").mouseenter(function(){
        $("#show1").css("background-color", "#f5f5f5");
    });
    $("#show1").mouseleave(function(){
        $("#show1").css("background-color", "white");
    });
});
</script>
<script>
$(document).ready(function(){
    $("#show2").mouseenter(function(){
        $("#show2").css("background-color", "#f5f5f5");
    });
    $("#show2").mouseleave(function(){
        $("#show2").css("background-color", "white");
    });
});
</script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>
