$(function(){
  $("#search").keyup(function(){
     var search = $("#search").val();
     $.ajax({
       type: "POST",
       url: "search.php",
       data: {"search": search},
       success: function(response){
          $("#result").html(response);
       }
     });
     return false;
   });
});