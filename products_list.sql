CREATE TABLE IF NOT EXISTS 'products_list' (
'id' INT(11) NOT NULL,
  'product_name' VARCHAR(60) NOT NULL,
  'product_desc' text NOT NULL,
  'product_code' VARCHAR(60) NOT NULL,
  'product_image' VARCHAR(60) NOT NULL,
  'product_price' DECIMAL(10,2) NOT NULL
) AUTO_INCREMENT=1 ;