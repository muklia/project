<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Homepage - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/shop-homepage.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/css.css">

    
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-phone"></span>Shop</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="index.php">Home</a>
                    </li>
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                    <li>
                      <?php 
                        session_start();
                        if(!empty($_SESSION['login'])){
                          echo "<a href = 'profile.php' class='' style=''>Login: ".$_SESSION['login']."</a>";
                        }else{
                          echo '<a href="sign1.php">Login or Register</a>';
                        }
                       ?>
                    </li>
                    <li>
                      <?php 
                      if(!empty($_SESSION['login'])){
                          echo "<a href = 'logout.php' class='' style=''>Logout</a>";
                        }
                      ?>
                    </li>
                    <li>
                        <?php 
                        $con = mysqli_connect('localhost','root','','users');
                        if (!empty($_SESSION['login'])){
                           $roww=$con->query("SELECT * FROM baskets WHERE login='".$_SESSION['login']."'");
                           $val=mysqli_num_rows($roww);
                         }
                        if(!empty($_SESSION['login'])){
                          if ($_SESSION['type']==0 || $_SESSION['type']==1){
                            echo  "<a href='korzina.php'><span class='glyphicon glyphicon-shopping-cart'></span> Your basket has ".$val." thing</a>";
                          }
                        }
                        else{
                          echo "<a href='#'><span class='glyphicon glyphicon-shopping-cart'></span>Your basket</a>";
                        };
                         ?>
                    </li>
                </ul>
                <!-- <a href="sign1.php"><button type="button" class="btn btn-default navbar-btn navbar-right">Sign</button></a>-->    
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <p class="lead">Shop Name</p>
                <div class="list-group">
                    <a href="index.php" class="list-group-item">Category 1</a>
                    <a href="#" class="list-group-item">Category 2</a>
                    <a href="#" class="list-group-item">Category 3</a>
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="img/apple111.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="img/carusel.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="img/carusel2.jpg" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php
              $x=0;
              $con=mysqli_connect("localhost","root","","users");
              $query="SELECT * FROM post";
              $results=$con->query($query);
              if (!empty($_SESSION['login'])){
                if($_SESSION['type']==1){
                  while($row = mysqli_fetch_array($results)){
                    $x=$x+1;
                    echo "<div class='col-sm-4 col-lg-4 col-md-4 pull-left'>
                        <div class='thumbnail'>
                            <img style='width:260px;height:146px'src='data:image;base64,".$row['imageofkit']."' alt=''>
                            <div class='caption'>
                                <h4 class='pull-right'>$".$row['price']."</h4>
                                <h4><a href='#'>".$row['namekit']."</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class='ratings'>
                                <button id='bask' style='text-align:center;margin-left:0px;font-size:12pt;font-weight:bold;font-style:italic;color:blue;' onclick='location.href=\"basket.php?&id=".$row['id']."\"'>to basket</button>
                                <button style='float:left;text-align:center;margin-left:0px;font-size:12pt;font-weight:bold;font-style:italic;color:blue;' onclick='location.href=\"delete.php?&id=".$row['id']."\"'>DELETE</button>
                                <button class='show' box='edit".(string) $x."' style='float:left;text-align:center;margin-left:0px;font-size:12pt;font-weight:bold;font-style:italic;color:blue;'>EDIT</button>
                                <div class='post' id='edit".(string) $x."'>
                                    <form action='edit.php' method='post' enctype='multipart/form-data'>
                                      <label for='nameinp' style='padding:7px;font-size:13pt;color:red;font-weight:bold;'>Name</label><input type='text' id='nameinp' name='namekit' style='width:280px;height:30px;font-size:12pt;font-style:italic;font-weight:bold;padding:7px;color:blue;margin-top:30px;' value='".$row['namekit']."'/><br>
                                      <label for='inpprice' style='padding:7px;font-size:13pt;color:red;font-weight:bold;'>Price</label><input type='text' id='inpprice' name='price' style='width:280px;height:30px;font-size:12pt;font-style:italic;font-weight:bold;padding:7px;color:blue;margin-left:5px;margin-top:5px;' value='".$row['price']."'/><br>
                                       <input type='file' name='image' style='margin-top:10px;margin-left:12px;color:red;font-size:12pt;' id='img' required/><br>
                                       <input type='hidden' name='id' value='".$row['id']."'/>
                                      <input type='submit' name='submitkit' style='margin-top:20px;margin-left:0px;font-size:13pt;color:red;font-weight:bold;background-color:yellow;border-radius:7px;' value='EDIT'/>
                                      <button class='close' style='height:25px;margin-left:30px;'>Закрыть</button>
                                    </form>
                                </div>
                            </div>
                            <div id='gopost'></div>
                        </div>
                    </div>";}
                  echo "<button class='show' box='create' style='margin-top:11px;'>CREATE</button>
                      <div class='post' id='create'>
                          <form action='forpost.php' method='post' enctype='multipart/form-data'>
                            <label for='inname' style='padding:7px;font-size:13pt;color:red;font-weight:bold;'>Name</label><input type='text' id='inname' name='namekit' style='width:280px;height:30px;font-size:12pt;font-style:italic;font-weight:bold;padding:7px;color:blue;margin-top:30px;'/><br>
                            <label for='inprice' style='padding:7px;font-size:13pt;color:red;font-weight:bold;'>Price</label><input type='text' id='inprice' name='price' style='width:280px;height:30px;font-size:12pt;font-style:italic;font-weight:bold;padding:7px;color:blue;margin-left:5px;margin-top:5px;'/></br>
                            <label for='indescription' style='padding:7px;font-size:13pt;color:black;font-weight:bold;'>Description</label><input type='text' id='indescription' name='description' style='width:230px;height:30px;font-size:12pt;font-style:italic;font-weight:bold;padding:7px;color:blue;margin-top:px;'/><br>
                             <input type='file' name='image' style='margin-top:10px;margin-left:12px;color:red;font-size:12pt;' id='img'/><br>
                            <input type='submit' name='submitkit' style='margin-top:0px;margin-left:90px;font-size:13pt;color:red;font-weight:bold;background-color:yellow;border-radius:7px;' value='POST'/>
                            <button class='close' style='height:25px;margin-left:30px;'>Закрыть</button>
                          </form>
                      </div>
                     <div id='gopost'></div>";
                }
                else if($_SESSION['type']==0){
                    while($row = mysqli_fetch_array($results)){
                  echo "<div class='col-sm-4 col-lg-4 col-md-4 pull-left'>
                        <div class='thumbnail'>
                            <img style='width:260px;height:146px'src='data:image;base64,".$row['imageofkit']."' alt=''>
                            <div class='caption'>
                                <h4 class='pull-right'>$".$row['price']."</h4>
                                <h4><a href='#'>".$row['namekit']."</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class='ratings'>
                                <button id='bask' style='text-align:center;margin-left:85px;font-size:12pt;font-weight:bold;font-style:italic;color:blue;' onclick='location.href=\"basket.php?&id=".$row['id']."\"'>to basket</button>
                            </div>
                        </div>
                        </div>";
                }
              }
            }
            else{
              while($row = mysqli_fetch_array($results)){
            echo "<div style='width:33%;float:left;height:450px;margin-top:15px;'>
              <a style='float:left;text-align:center;margin-left:45px;font-size:12pt;font-weight:bold;font-style:italic;color:blue;'>".$row['namekit']."</a><br></br>
              <img width='240px' height='235px' src='data:image;base64,".$row['imageofkit']."' style='border-radius:30px;padding:5px;'/><br></br>
              <a style='float:left;text-align:center;margin-left:90px;font-size:12pt;font-weight:bold;font-style:italic;color:blue;'>price:".$row['price']."</a><br></br>
              <a style='padding:7px;color:red;font-weight:bold;font-size:13px;margin-left:5px;'>If you want to add this object please log in</a>
            </div>";
          }
            }

              ?>
                    <!-- <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="img/iphone5c.png" alt="">
                            <div class="caption">
                                <h4 class="pull-right">$74.99</h4>
                                <h4><a href="#">Third Product</a>
                                </h4>
                                <p>This is a short description. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                            <div class="ratings">
                                <p class="pull-right">31 reviews</p>
                                <p>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </p>
                            </div>
                        </div>
                    </div> -->
                </div>
                
                    

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Shop 2016</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery-2.2.0.js"></script>


    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/post.js"></script>

</body>
</html>